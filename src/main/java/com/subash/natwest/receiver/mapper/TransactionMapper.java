package com.subash.natwest.receiver.mapper;

import com.subash.natwest.receiver.dto.Transaction;
import com.subash.natwest.receiver.entity.TransactionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface TransactionMapper {

    static TransactionMapper getInstance() {
        return Mappers.getMapper(TransactionMapper.class);
    }

    TransactionEntity mapTransactionToTransactionEntity(Transaction transaction);
}
