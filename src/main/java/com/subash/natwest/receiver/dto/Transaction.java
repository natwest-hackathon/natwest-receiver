package com.subash.natwest.receiver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Transaction
 */
@Setter
@Getter
@ToString
public class Transaction {

    @JsonProperty("accountNumber")
    private String accountNumber;

    @JsonProperty("amount")
    private Double amount;

    @JsonProperty("type")
    private String type;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("accountFrom")
    private String accountFrom;
}

