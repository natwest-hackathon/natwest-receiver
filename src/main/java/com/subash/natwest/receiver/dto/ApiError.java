package com.subash.natwest.receiver.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ApiError implements Serializable {
    private static final long serialVersionUID = 1L;
    private HttpStatus status;
    private String error;
    private Integer count;
    private List<String> errors;
}

