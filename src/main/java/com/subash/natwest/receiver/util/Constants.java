package com.subash.natwest.receiver.util;

public class Constants {

    // Constants for logger
    public final static String LOG_APP_NAME = "NATWEST-RECEIVER";
    public final static String LOG_OPERATION_ID = "[OPERATION ID] : ";
    public final static String LOG_METHOD = "[HTTP METHOD] : ";
    public final static String LOG_REQUEST = "[REQUEST BODY] : ";
    public final static String LOG_RESPONSE = "[RESPONSE BODY] : ";
    public final static String LOG_FAILURE_MSG = "[FAILED TO LOG] : ";
    public final static String LOG_UUID = "[UUID] : ";
    public final static String LOG_STATUS = "[STATUS] : ";
    public final static String LOG_APP = "[APPLICATION] : ";

    public final static String ERROR_MESSAGE = "[ERROR MESSAGE] : ";

    public final static String STACK_TRACE = "[FULL STACK TRACE] : ";


    // OperationId
    public final static String SAVE_TRANSACTION = "saveTransaction";

    // Response Code
    public final static String DECRYPT_SUCCESS_CODE = "NR-30001";
    public final static String DECRYPT_SUCCESS_DESC = "Decrypted and Saved Successfully";

    public final static String DECRYPT_ERROR_CODE = "NR-40001";
    public final static String DECRYPT_ERROR_DESC = "Failed to decrypt";

    public final static String SECRET_KEY = "natwe$t-tr@ns@ctions";
}
