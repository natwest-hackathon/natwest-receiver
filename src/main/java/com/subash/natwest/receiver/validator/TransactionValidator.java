package com.subash.natwest.receiver.validator;


import com.subash.natwest.receiver.model.EncryptedTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Locale;

@Component
public class TransactionValidator implements Validator {


    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class<?> clazz) {
        return EncryptedTransaction.class.equals(clazz);
    }

    @Override
    public void validate(Object transaction, Errors errors) {

        //transactionString
        ValidationUtils.rejectIfEmpty(errors, "transactionString",
                messageSource.getMessage("natwest.receiver.transactionString.accountNumber.error", null, Locale.getDefault()));

    }
}
