package com.subash.natwest.receiver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class NatwestReceiverApplication {

	public static void main(String[] args) {
		SpringApplication.run(NatwestReceiverApplication.class, args);
	}
}
