package com.subash.natwest.receiver.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.subash.natwest.receiver.dto.Transaction;
import com.subash.natwest.receiver.entity.TransactionEntity;
import com.subash.natwest.receiver.mapper.TransactionMapper;
import com.subash.natwest.receiver.model.EncryptedTransaction;
import com.subash.natwest.receiver.model.GenericResponse;
import com.subash.natwest.receiver.repository.TransactionRepository;
import com.subash.natwest.receiver.util.AESUtil;
import com.subash.natwest.receiver.util.GenericLogger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import static com.subash.natwest.receiver.util.Constants.*;

@Component
public class ReceiverService {

    private static final Logger logger = LogManager.getLogger(ReceiverService.class);

    TransactionMapper transactionMapper = TransactionMapper.getInstance();

    @Autowired
    TransactionRepository repository;

    /**
     * Method helps to save Transaction
     *
     * @param UUID
     * @param encryptedTransaction
     * @return
     */
    public ResponseEntity<GenericResponse> decryptAndSaveTransaction(String UUID, EncryptedTransaction encryptedTransaction) {
        GenericResponse response = new GenericResponse();
        try {
            // Decrypt logic
            String transactionDecrypted = AESUtil.decrypt(encryptedTransaction.getTransactionString(), SECRET_KEY);
            ObjectMapper mapper = new ObjectMapper();
            Transaction transaction = mapper.readValue(transactionDecrypted, Transaction.class);
            logger.info("Decrypted Transaction : " + transaction.toString());

            // Mapper
            TransactionEntity transactionEntity = transactionMapper.mapTransactionToTransactionEntity(transaction);
            repository.save(transactionEntity);

            response.code(DECRYPT_SUCCESS_CODE);
            response.description(DECRYPT_SUCCESS_DESC);
            GenericLogger.logResponse(logger, UUID, "SUCCESS", response);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            GenericLogger.logError(logger, UUID, "ERROR", e);
            // Set and log response
            response.code(DECRYPT_ERROR_CODE);
            response.description(DECRYPT_ERROR_DESC);
            GenericLogger.logResponse(logger, UUID, "ERROR", response);
            return ResponseEntity.internalServerError().build();
        }
    }
}
