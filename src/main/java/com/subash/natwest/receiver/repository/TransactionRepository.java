package com.subash.natwest.receiver.repository;

import com.subash.natwest.receiver.entity.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<TransactionEntity, Integer> {


}
