package com.subash.natwest.receiver.controller;

import com.subash.natwest.receiver.model.EncryptedTransaction;
import com.subash.natwest.receiver.model.GenericResponse;
import com.subash.natwest.receiver.service.ReceiverService;
import com.subash.natwest.receiver.util.Constants;
import com.subash.natwest.receiver.util.GenericLogger;
import com.subash.natwest.receiver.validator.TransactionValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class ReceiverController implements SavetransactionApi {

    private static final Logger logger = LogManager.getLogger(ReceiverController.class);

    @Autowired
    private TransactionValidator transactionValidator;

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.setValidator(transactionValidator);
    }

    @Autowired
    private ReceiverService service;

    @Override
    public ResponseEntity<GenericResponse> saveTransaction(EncryptedTransaction encryptedTransaction) {
        String uuid = GenericLogger.getUUID();
        GenericLogger.logRequest(logger, uuid, Constants.SAVE_TRANSACTION, "POST", encryptedTransaction);
        return service.decryptAndSaveTransaction(uuid, encryptedTransaction);
    }

}
