FROM adoptopenjdk/openjdk16:alpine-jre
ADD target/natwest-receiver-0.0.1-SNAPSHOT.jar natwest-receiver.jar
ENTRYPOINT ["java","-jar","natwest-receiver.jar"]
